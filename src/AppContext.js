import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { populateDummyData } from "utils/stubs";

const AppContext = React.createContext();
const AppProvider = ({ children }) => {
  const [isLoggedIn, setLoggedIn] = useState(
    window.localStorage.getItem("isLoggedIn")
  );
  const history = useHistory();
  const login = () => {
    window.localStorage.setItem("isLoggedIn", true);
    populateDummyData();
    setLoggedIn(true);
    history.push("/workflows");
  };
  const context = {
    isLoggedIn,
    login,
  };
  return <AppContext.Provider value={context}>{children}</AppContext.Provider>;
};

export { AppContext, AppProvider };
