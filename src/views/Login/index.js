import React, { useContext, useState } from "react";
import {
  Card,
  CardBody,
  CardTitle,
  CardFooter,
  InputGroup,
  InputGroupAddon,
  Input,
  Button,
} from "reactstrap";
import { AppContext } from "AppContext";
import style from "./_login.module.scss";
const Login = () => {
  const { login } = useContext(AppContext);
  const [processing, setProcessing] = useState(false);
  const onSubmit = () => {
    setProcessing(true);
    setTimeout(function () {
      login();
    }, 2000);
  };
  return (
    <Card className={style.wrapper}>
      <CardTitle className={style.heading}>Login</CardTitle>
      <CardBody>
        <InputGroup className={style.group}>
          <InputGroupAddon addonType="prepend" className={style.prepend}>
            <i className="far fa-envelope"></i>
          </InputGroupAddon>
          <Input placeholder="Email" type="email" disabled={processing} />
        </InputGroup>

        <InputGroup className={style.group}>
          <InputGroupAddon addonType="prepend" className={style.prepend}>
            <i className="fas fa-key"></i>
          </InputGroupAddon>
          <Input placeholder="Password" type="password" disabled={processing} />
        </InputGroup>

        <Button
          className={style.action}
          color="primary"
          onClick={onSubmit}
          disabled={processing}
        >
          {processing ? "Processing..." : "Login"}
        </Button>
      </CardBody>
      <CardFooter className={style.footer}>
        Don't have an account ? Sign up here.
      </CardFooter>
    </Card>
  );
};
export default Login;
