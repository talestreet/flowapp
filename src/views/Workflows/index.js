import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Grid from "./components/Grid";
import WorkflowView from "./components/WorkflowView";
const Workflows = () => {
  return (
    <Switch>
      <Route exact="true" path="/workflows">
        <Grid />
      </Route>
      <Route exact="true" path="/workflows/:id">
        <WorkflowView />
      </Route>
      <Redirect to="/404" />
    </Switch>
  );
};
export default Workflows;
