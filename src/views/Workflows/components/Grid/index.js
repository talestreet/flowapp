import React, { useState, useEffect } from "react";
import {
  Row,
  Col,
  Button,
  InputGroup,
  InputGroupAddon,
  Input,
} from "reactstrap";
import { WorkflowTile } from "components";
import { getAllWorkflows, updateWorkflows, areNodesCompleted } from "utils";
import style from "./_grid.module.scss";
import { Link } from "react-router-dom";
import Select from "react-select";
import { toast } from "react-toastify";
const options = [
  { value: "all", label: "All" },
  { value: "completed", label: "Completed" },
  { value: "pending", label: "Pending" },
];
const Grid = () => {
  const [list, setList] = useState([]);
  const [filterValue, setFilterValue] = useState({
    value: "all",
    label: "All",
  });
  const [searchKey, setSearchKey] = useState(null);
  useEffect(() => {
    const { value } = filterValue;
    if (value !== "all") {
      setList(getAllWorkflows().filter(({ status }) => status === value));
    } else {
      setList(getAllWorkflows());
    }
  }, [filterValue]);
  const toggleStatus = ({ id: workflowId, status }) => {
    const newStatus = status === "pending" ? "completed" : "pending";

    const index = list.findIndex(({ id }) => id === workflowId);
    if (index > -1) {
      const data = [...list];

      if (newStatus === "completed" && !areNodesCompleted(data[index].nodes)) {
        toast.error(
          "Please complete all the nodes in the workflow to proceed."
        );
        return;
      }
      data[index]["status"] = newStatus;
      updateWorkflows(data);
      setList(data);
    }
  };

  const remove = (workflowId) => {
    const index = list.findIndex(({ id }) => id === workflowId);
    const data = [...list];
    if (index > -1) {
      data.splice(index, 1);
      updateWorkflows(data);
      setList(data);
    }
  };
  const workflows = list
    .filter(({ name }) => {
      if (searchKey && searchKey.length > 0) {
        return name.indexOf(searchKey) > -1;
      }
      return true;
    })
    .map((data) => {
      return (
        <Col lg="3" className="mb-5">
          <WorkflowTile {...data} toggleStatus={toggleStatus} remove={remove} />
        </Col>
      );
    });

  return (
    <div>
      <Row className={style.header}>
        <Col lg="3" md="3">
          <InputGroup className={style.group}>
            <InputGroupAddon addonType="prepend" className={style.prepend}>
              <i className="fas fa-search"></i>
            </InputGroupAddon>
            <Input
              placeholder="Search Workflows"
              type="text"
              onChange={(e) => {
                setSearchKey(e.target.value);
              }}
            />
          </InputGroup>
        </Col>
        <Col lg="3" md="3" className={style.filter}>
          {" "}
          <Select
            options={options}
            value={filterValue}
            onChange={setFilterValue}
          />
        </Col>
        <Col lg="6" md="6" className={style.createButton}>
          {" "}
          <Link to="/workflows/create">
            <Button color="success">
              {" "}
              <i className="fas fa-plus"></i> Create Workflow
            </Button>
          </Link>
        </Col>
      </Row>
      <Row className="mt-4">{workflows}</Row>
    </div>
  );
};

export default Grid;
