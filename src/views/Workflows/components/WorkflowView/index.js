import React, { useState, useEffect } from "react";
import { useParams, useHistory } from "react-router-dom";
import { getWorkflow, updateWorkflow, nextStatusMap } from "utils";
import { Row, Col, Input, InputGroup } from "reactstrap";
import style from "./_workflowView.module.scss";
import Actions from "./Actions";
import { Node } from "components";
import { areNodesCompleted, validateWorkflow, shuffle } from "utils";
import { toast } from "react-toastify";
import _get from "lodash/get";
const workflowDraftData = {
  status: "pending",
  nodes: [],
};
const WorkflowView = () => {
  const { id } = useParams();
  const history = useHistory();
  const [errors, setErrors] = useState(null);
  const workflowData = id === "create" ? workflowDraftData : getWorkflow(id);
  const [data, setData] = useState(workflowData);
  useEffect(() => {
    setErrors(null);
  }, [data]);
  const toggleStatus = (nodeId) => {
    const nodes = [...data.nodes];
    const index = nodes.findIndex(({ id }) => id === nodeId);
    if (index > -1) {
      const status = nodes[index].status;
      const nextStatus = nextStatusMap[status];
      const shouldCheckPreviousNodes = index > 0 && nextStatus === "completed";
      if (
        !shouldCheckPreviousNodes ||
        areNodesCompleted(nodes.slice(0, index))
      ) {
        nodes[index] = { ...nodes[index], status: nextStatus };
        setData({ ...data, nodes });
      } else {
        toast.error("Nodes should be completed in sequential order only.");
      }
    }
  };
  const updateNode = ({ id: nodeId, _key, value }) => {
    const nodes = [...data.nodes];
    const index = nodes.findIndex(({ id }) => id === nodeId);
    if (index > -1) {
      nodes[index][_key] = value;
      setData({ ...data, nodes });
    }
  };

  const addNode = () => {
    const nodes = data.nodes ? [...data.nodes] : [];
    nodes.push({
      id: `node${Date.now()}`,
      title: `Task${nodes.length + 1}`,
      status: "pending",
      content: "",
    });
    setData({
      ...data,
      nodes,
    });
  };
  const deleteNode = () => {
    const nodes = [...data.nodes];
    nodes.pop();
    setData({
      ...data,
      nodes,
    });
  };
  const handleWorkflowName = (e) => {
    setData({ ...data, name: e.target.value });
  };
  const save = () => {
    const errors = validateWorkflow(data);
    console.log("Errors", errors);
    if (Object.keys(errors).length > 0) {
      toast.error("Please see highlighted fields for errors.");
      setErrors(errors);
      return;
    }
    const newWorkflowId = updateWorkflow(data);
    if (newWorkflowId) {
      history.replace(`/workflows/${newWorkflowId}`);
      toast.success("Sucessfully created new workflow.");
    } else {
      toast.success("Sucessfully saved the workflow.");
    }
  };
  const shuffleNodes = () => {
    const nodes = data.nodes ? [...data.nodes] : [];
    setData({ ...data, nodes: shuffle(nodes) });
  };
  if (!workflowData) return "No workflow found";
  const workflowLocked = workflowData.status === "completed";
  return (
    <div>
      <Row className={style.header}>
        <Col lg="3" md="3">
          {" "}
          <InputGroup>
            <Input
              onChange={handleWorkflowName}
              value={data.name}
              placeholder="Workflow Name"
              className={_get(errors, "name") && "is-invalid"}
            />
            {_get(errors, "name") && (
              <div className="invalid-tooltip">{errors.name}</div>
            )}
          </InputGroup>
        </Col>
        <Col sm="12" md="9" className={style.actions}>
          <Actions
            addNode={addNode}
            deleteNode={deleteNode}
            save={save}
            workflowLocked={workflowLocked}
            shuffle={shuffleNodes}
          />
        </Col>
      </Row>
      <div className={style.nodes}>
        {data.nodes &&
          data.nodes.map((node) => (
            <Node
              {...node}
              toggleStatus={toggleStatus}
              update={updateNode}
              workflowLocked={workflowLocked}
              errors={_get(errors, `nodes.${node.id}`)}
            />
          ))}
      </div>
    </div>
  );
};

export default WorkflowView;
