import React from "react";
import { Button } from "reactstrap";
import style from "./_workflowView.module.scss";
import { If } from "utils";
const Actions = ({ addNode, deleteNode, save, shuffle, workflowLocked }) => {
  return (
    <>
      <If test={workflowLocked}>
        <Button className={style.action} color="success" onClick={shuffle}>
          <i className="fas fa-random"></i>Shuffle
        </Button>
      </If>
      <Button
        disabled={workflowLocked}
        className={style.action}
        color="success"
        onClick={addNode}
      >
        <i className="fas fa-plus"></i> Add Node
      </Button>

      <Button
        disabled={workflowLocked}
        className={style.action}
        color="danger"
        onClick={deleteNode}
      >
        <i className="fas fa-times"></i> Delete Node
      </Button>

      <Button className={style.action} color="primary" onClick={save}>
        Save
      </Button>
    </>
  );
};
export default Actions;
