import React, { useContext } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Loadable from "react-loadable";
import { AppContext } from "./AppContext";
import { DefaultLayout, WithHeaderLayout } from "layout";
import { Loader } from "components";
const Login = Loadable({
  loader: () => import("views/Login"),
  loading: Loader,
});
const Workflows = Loadable({
  loader: () => import("views/Workflows"),
  loading: Loader,
});
const NotFound = Loadable({
  loader: () => import("views/NotFound"),
  loading: Loader,
});
function App() {
  const { isLoggedIn } = useContext(AppContext);
  return (
    <Switch>
      <Route
        exact="true"
        path="/login"
        render={() => {
          if (isLoggedIn) return <Redirect to="/workflows" />;
          return (
            <DefaultLayout>
              <Login />
            </DefaultLayout>
          );
        }}
      />

      <Route
        path="/workflows"
        render={() => {
          if (!isLoggedIn) return <Redirect to="/login" />;
          return (
            <WithHeaderLayout>
              <Workflows />
            </WithHeaderLayout>
          );
        }}
      />

      <Route exact="true" path="/404">
        <WithHeaderLayout>
          <NotFound />
        </WithHeaderLayout>
      </Route>
      <Redirect from="/" to="/workflows" />
      <Redirect to="/404" />
    </Switch>
  );
}

export default App;
