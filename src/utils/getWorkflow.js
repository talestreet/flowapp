const getWorkflow = (workflowId) => {
  const workflows = JSON.parse(window.localStorage.getItem("workflows"));
  if (!workflows) return null;
  return workflows.find(({ id }) => id === workflowId);
};
export default getWorkflow;
