const areNodesCompleted = (nodes) => {
  if (!nodes) nodes = [];
  for (var i = 0; i < nodes.length; i++) {
    if (nodes[i].status !== "completed") {
      return false;
    }
  }
  return true;
};
export default areNodesCompleted;
