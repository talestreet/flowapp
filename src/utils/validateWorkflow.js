const validateWorkflow = (data) => {
  console.log("v", data);
  let errors = {};
  if (!data.name || data.name.length < 1) {
    errors.name = "*Required";
  }
  let nodes =
    data.nodes &&
    data.nodes.reduce((acc, node) => {
      if (!node.title || node.title.length < 1) {
        acc[node.id] = {
          title: "*Required",
        };
      }
      if (!node.content || node.content.length < 1) {
        if (acc[node.id]) {
          acc[node.id].content = "*Required";
        } else {
          acc[node.id] = {
            content: "*Required",
          };
        }
      }
      return acc;
    }, {});
  if (Object.keys(nodes).length > 0) {
    errors.nodes = nodes;
  }
  return errors;
};
export default validateWorkflow;
