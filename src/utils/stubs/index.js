const workflows = [
  {
    id: "w1",
    name: "Workflow 1",
    status: "pending",
    nodes: [
      {
        id: "node1",
        title: "Task1",
        status: "pending",
        content: "Pending",
      },
      {
        id: "node2",
        title: "Task2",
        status: "in-progress",
        content: "In progress",
      },
      {
        id: "node3",
        title: "Task3",
        status: "pending",
        content: "Pending",
      },
    ],
  },
];

const populateDummyData = () => {
  window.localStorage.setItem("workflows", JSON.stringify(workflows));
};
export { populateDummyData };
