const updateWorkflows = (data) => {
  window.localStorage.setItem("workflows", JSON.stringify(data));
};
export default updateWorkflows;
