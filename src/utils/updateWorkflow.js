const updateWorkflow = (data) => {
  if (data.id) {
    const workflows = JSON.parse(window.localStorage.getItem("workflows"));
    if (!workflows) return null;
    const index = workflows.findIndex(({ id }) => id === data.id);
    workflows[index] = data;
    window.localStorage.setItem("workflows", JSON.stringify(workflows));
    return null;
  } else {
    const workflows =
      JSON.parse(window.localStorage.getItem("workflows")) || [];
    data.id = "w__" + Date.now();
    workflows.push(data);
    window.localStorage.setItem("workflows", JSON.stringify(workflows));
    return data.id; //return id when creating a new workflow
  }
};
export default updateWorkflow;
