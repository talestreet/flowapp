import If from "./If";
import shuffle from "./shuffle";
import validateWorkflow from "./validateWorkflow";
import getWorkflow from "./getWorkflow";
import getAllWorkflows from "./getAllWorkflows";
import updateWorkflow from "./updateWorkflow";
import updateWorkflows from "./updateWorkflows";
import areNodesCompleted from "./areNodesCompleted";
const nextStatusMap = {
  "in-progress": "completed",
  completed: "pending",
  pending: "in-progress",
};
const statusColorMap = {
  "in-progress": "primary",
  completed: "success",
  pending: "secondary",
};

export {
  If,
  getWorkflow,
  getAllWorkflows,
  updateWorkflow,
  nextStatusMap,
  statusColorMap,
  updateWorkflows,
  areNodesCompleted,
  validateWorkflow,
  shuffle,
};
