const getAllWorkflows = () => {
  return JSON.parse(window.localStorage.getItem("workflows")) || [];
};
export default getAllWorkflows;
