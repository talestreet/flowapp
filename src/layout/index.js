import DefaultLayout from './DefaultLayout';
import WithHeaderLayout from './WithHeaderLayout';
export {
  DefaultLayout,
  WithHeaderLayout
}
