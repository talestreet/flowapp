import React from 'react';
import {
  Container
} from 'reactstrap';
import {Header} from 'components';

const WithHeaderLayout=({children})=>{
  return (
    <Container fluid>
      <Header/>
      {children}
    </Container>
  )
}
export default WithHeaderLayout;
