import React from 'react';
import {Container} from 'reactstrap';
const DefaultLayout=({children})=>{
  return (
    <Container>
      {children}
    </Container>
  )
}
export default DefaultLayout;
