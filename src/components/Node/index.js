import React from "react";
import style from "./_node.module.scss";
import { statusColorMap } from "utils";
import { Card, Button, Input, InputGroup } from "reactstrap";
import _get from "lodash/get";
var classnames = require("classnames");
const Node = ({
  title,
  status,
  id,
  toggleStatus,
  content,
  update,
  errors,
  workflowLocked,
}) => {
  return (
    <Card className={style.node}>
      <InputGroup>
        <Input
          placeholder="Node Title"
          className={classnames({
            [style.title]: true,
            "is-invalid": errors && errors.title,
          })}
          value={title}
          onChange={(e) => update({ id, _key: "title", value: e.target.value })}
        />
        {_get(errors, "title") && (
          <div className="invalid-tooltip">{errors.title}</div>
        )}
      </InputGroup>
      <InputGroup>
        <Input
          placeholder="Node Content"
          className={classnames({
            [style.content]: true,
            "is-invalid": errors && errors.content,
          })}
          type="textarea"
          value={content}
          onChange={(e) =>
            update({ id, _key: "content", value: e.target.value })
          }
        />
        {_get(errors, "content") && (
          <div className="invalid-tooltip">{errors.content}</div>
        )}
      </InputGroup>
      <Button
        className={style.check}
        color={statusColorMap[status]}
        disabled={workflowLocked}
        onClick={() => {
          toggleStatus(id);
        }}
      >
        <i className="fas fa-check"></i>
      </Button>
    </Card>
  );
};

export default Node;
