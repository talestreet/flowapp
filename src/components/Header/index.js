import React from "react";
import { Navbar, NavbarBrand, Button } from "reactstrap";
import style from "./_header.module.scss";
const Header = () => {
  const logout = () => {
    window.localStorage.removeItem("isLoggedIn");
    window.localStorage.removeItem("workflows");
    window.location.reload();
  };
  return (
    <Navbar className={style.wrapper}>
      <NavbarBrand className={style.brandName} href="/">
        FlowApp
      </NavbarBrand>
      <Button className={style.logout} onClick={logout}>
        Logout
      </Button>
    </Navbar>
  );
};
export default Header;
