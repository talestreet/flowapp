import React from "react";
import { Card, CardBody, Button, Row, Col } from "reactstrap";
import { Link } from "react-router-dom";
import style from "./_workflowTile.module.scss";
var classnames = require("classnames");
const WorkflowTile = ({ name, status, id, toggleStatus, remove, nodes }) => {
  return (
    <Card className={style.tile}>
      <CardBody>
        <Link to={`/workflows/${id}`}>
          <h6 className={style.title}>{name}</h6>
        </Link>
        <Button
          color="danger"
          onClick={() => {
            remove(id);
          }}
          className={classnames({
            [style.deleteButton]: true,
            "circular-button": true,
          })}
        >
          <i className="fas fa-trash"></i>
        </Button>
        <Row className="mt-3">
          <Col className="text-capitalize">{status}</Col>
          <Col className="text-right">
            <Button
              className={classnames({
                [style.toggleButton]: true,
                "circular-button": true,
              })}
              color={status === "pending" ? "secondary" : "success"}
              onClick={() => {
                toggleStatus({ id, status });
              }}
            >
              <i className="fas fa-check"></i>
            </Button>
          </Col>
        </Row>
      </CardBody>
    </Card>
  );
};
export default WorkflowTile;
