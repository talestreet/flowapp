import React from "react";
import ReactDOM from "react-dom";
import "./style/_index.scss";
import App from "./App";
import { BrowserRouter as Router } from "react-router-dom";
import { AppProvider } from "./AppContext";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
ReactDOM.render(
  <React.StrictMode>
    <Router>
      <AppProvider>
        <ToastContainer position="bottom-right" />
        <App />
      </AppProvider>
    </Router>
  </React.StrictMode>,
  document.getElementById("root")
);
